
### Installing
Make sure you already installed virtual environment for django in your computer.
```
virtualenv environment
environment\scripts\activate
pip install -r requirements.txt
npm install
python manage.py runserver

# open another terminal
npm start

open to your browser: localhost:3000
```