import React from 'react'
import {
    Button,
    Header,
    Footer
} from './components';

export default class App extends React.Component{
    render(){
        return(
            <div>
                <Header />
                <Button />
                <Footer />

            </div>
        )
    }
    
}