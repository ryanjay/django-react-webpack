## SRC

`src` is where all dynamic files reside.  If the file is imported by the javascript application it goes in this folder.  Webpack will give files within this folder a unique name in the production build so the client downloads the latest version rather than relying on a cached version. 
 
 `components` hold all dynamically changing components to be used in `routes`
 
 `helpers` holds all helper functions that are used globally.  Keep it DRY by importing helper functions to components or other helpers functions only when needed.
 
 `routes` are where each view is defined and created.  Need to look into this more.
 
 `scss` is where all globally stylesheets are located.  They should be organized using the ITCSS structure and conform to the BEM naming convention.