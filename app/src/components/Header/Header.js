import React from 'react';
import HeaderNav from "./HeaderNav/HeaderNav";

const Header = () => {
    return (
        <div>
            <h1>Header</h1>
            <HeaderNav/>
        </div>
    )
};

export default Header