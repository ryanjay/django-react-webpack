## Components

Components are what make up the application.  We organize them by their name, with a capital letter such as `Header` to distinguish them from an element.  Each Component folder will have a `Component.js` and a `Component.scss` file that holds all compilable code related to that Component. 

```
Header
-- Header.js
-- Header.scss
```

If a Component becomes to large and cumbersome, and can be split into Child Components, we store those within the Component's folder...

```$xslt
Header
-- HeaderNav
--- HeaderNav.js
--- HeaderNav.scss
-- Header.js
-- Header.scss
```

This Child Component is ONLY used within the immediate Parent Component and is not exported beyond the Parent's folder.

All Components, that are not Child Components, are exported to an `index.js`.  This allows us to import components in our `routes`.  

```$xslt
export { default as Button } from './Button/Button';
export { default as Header } from './Header/Header';
export { default as Footer } from './Footer/Footer';
```

We should try to keep this in alphabetical order to make it easy to read and target a component.

If a Component needs to be used within another Component, but is not an immediate child, a simple import of that Component will do.

```$xslt
import Button from '../Button/Button; // directly to the component

or

import { Button } from '../'; // component root
```