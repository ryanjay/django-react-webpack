var path = require("path")
var webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var BundleTracker = require('webpack-bundle-tracker')
// new WebpackDevServer(webpack(config), {

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: {
        main: path.resolve(__dirname, './app/src')
    },
    output: {
        path: path.resolve(__dirname, './app/static/bundles'),
        filename: '[name]-[hash].js',
        publicPath: path.resolve(__dirname, './app/static/bundles')
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            // chunkFilename: '[id].css'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new BundleTracker({filename: './webpack-stats.json'}), // Needed to match django webpack bundle in home.html
    ],
    module: {
        rules: [
            {
                test: /\.jsx$|\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {}
                },
                exclude: /(node_modules|bower_components)/
            }
        ]
    },
    devServer: {
        historyApiFallback: true,
        open: 'chrome',
        compress: true,
        hot: true,
        liveReload: true,
        // publicPath: path.resolve(__dirname, 'app/static/bundles'),
        writeToDisk: true,
        port: 3000,
        proxy: {
            '!/app/static/**': { // to bundle
                target: 'http://localhost:8000', // points to django dev server
                changeOrigin: true,
            },
        }
    }




  //
  // context: __dirname,
  // entry: [
  //     'webpack-dev-server/client?http://localhost:3000',
  //     'webpack/hot/only-dev-server',
  //     './app/modules/index'
  // ],
  //
  // output: {
  //     path: path.resolve('./app/static/bundles/'),
  //     filename: '[name]-[hash].js',
  //     publicPath: 'http://localhost:3000/app/static/bundles/',
  //      // Tell django to use this URL to load packages and not use STATIC_URL + bundle_name
  // },
  //
  // plugins: [
  //   new webpack.HotModuleReplacementPlugin(),
  //   new webpack.NoErrorsPlugin(), // don't reload if there is an error
  //   new BundleTracker({filename: './webpack-stats.json'}),
  // ],
  //
  // module: {
  //   loaders: [
  //     // we pass the output from babel loader to react-hot loader
  //     { test: /\.jsx?$/, exclude: /node_modules/,
  //       loaders: ['react-hot-loader/webpack', 'babel?' + JSON.stringify({presets: ['react', 'es2015']})]
  //
  //   },
  //   ],
  // },
  //
  // resolve: {
  //   modulesDirectories: ['node_modules', 'bower_components'],
  //   extensions: ['', '.js', '.jsx']
  // }
}